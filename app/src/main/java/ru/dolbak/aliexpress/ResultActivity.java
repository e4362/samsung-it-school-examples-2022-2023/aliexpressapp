package ru.dolbak.aliexpress;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.util.Locale;

public class ResultActivity extends AppCompatActivity {
    String firstPart, secondPart;
    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        textView = findViewById(R.id.textView2);
        String name = getIntent().getStringExtra("name");
        int day = getIntent().getIntExtra("day", 0);
        String[] names = getResources().getStringArray(R.array.names);
        String[] days = getResources().getStringArray(R.array.days);
        for (String text : names){
            if (text.toLowerCase(Locale.ROOT).charAt(0) ==
            name.toLowerCase(Locale.ROOT).charAt(0)){
                firstPart = text.substring(1);
            }
        }
        secondPart = days[day - 1];
        textView.setText(firstPart + " " + secondPart.toLowerCase(Locale.ROOT));
    }

    public void onClick2(View view) {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, "Я скачал(а) приложение Aliexpress от Никиты Долбак и узнал," +
                " что если бы меня выставили на продажу, то я бы был " + firstPart.toLowerCase(Locale.ROOT)
                + " " + secondPart.toLowerCase(Locale.ROOT));
        sendIntent.setType("text/plain");

        Intent shareIntent = Intent.createChooser(sendIntent, null);
        startActivity(shareIntent);
    }
}
package ru.dolbak.aliexpress;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
    EditText editName;
    DatePicker datePicker;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        editName = findViewById(R.id.editTextTextPersonName);
        datePicker = findViewById(R.id.date_picker);
        Log.d("NIKITA", "onCreate Loaded!");
    }


    public void onClick(View view) {
        String name = editName.getText().toString();
        int day = datePicker.getDayOfMonth();
        Intent intent = new Intent(MainActivity.this, LoadingScreen.class);
        intent.putExtra("name", name);
        intent.putExtra("day", day);
        startActivity(intent);
    }
}